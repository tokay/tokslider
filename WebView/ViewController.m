//
//  ViewController.m
//  WebView
//
//  Created by Chris Air on 8/21/12.
//  Copyright (c) 2012 Chris Air. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController
@synthesize viewWeb;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // get localized path for file from app bundle
	
    NSString *path = [[NSBundle mainBundle] pathForResource:@"index" ofType:@"html" inDirectory:@"www"];
//    NSString *path = [[NSBundle mainBundle] pathForResource:@"holder" ofType:@"html" inDirectory:@"www"];
    NSURL *url = [NSURL fileURLWithPath:path];
    
    // uncomment to call external website
    
    //    NSURL *url = [NSURL URLWithString:@"http://lab.hakim.se/meny/"];

	
    // set up the webview
    
    [viewWeb setOpaque:NO];
    [viewWeb setBackgroundColor:[UIColor clearColor]];
    
    // Start of Stop Scroll & Bounce
    
    //    http://www.iphonedevsdk.com/forum/iphone-sdk-development/996-turn-off-scrolling-bounces-uiwebview.html
    
    id scrollview = [[viewWeb subviews] lastObject];
    if ([(UIScrollView *)scrollview respondsToSelector:@selector(setScrollEnabled)])
    {
        [(UIScrollView *)scrollview setScrollEnabled:NO];
    }
    
    //    http://stackoverflow.com/questions/500761/stop-uiwebview-from-bouncing-vertically
    
    viewWeb.scrollView.bounces = NO;
    
    viewWeb.scalesPageToFit = NO;
    
    // End of Stop Scroll & Bounce
    
     
    
    [viewWeb loadRequest:[NSURLRequest requestWithURL:url]];
}

- (void)viewDidUnload
{
    [self setViewWeb:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
    } else {
        return YES;
    }
}

// Hmmm see http://stackoverflow.com/questions/12404556/interface-orientation-in-ios-6-0?rq=1
// 


//-(BOOL)shouldAutorotate
//{
//    return YES;
//}
//
//-(NSUInteger)supportedInterfaceOrientations
//{
//    return UIInterfaceOrientationMaskAll;
//}
//
//- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
//{
//    return UIInterfaceOrientationMaskPortrait;
//}
@end
