$(function() {
    // setup Fastclick 

    window.addEventListener('load', function() {
        new FastClick(document.body);
    }, false);

    $("#loader").loader();        // load manifest ans current slideSet

    // Initialize the slider itself

    var sliderElement; 
    sliderElement = $("div#js-tokslider").css('overflow', 'hidden').children('ul');
    sliderElement.packSlider(sliderElement, $("#js-tokslider-nav"));
    
    $("#packMenu").packMenu({   // init the main PackMenu
        transition:         'slideToggle',
        transitionTimer: 750,
        menuButton: null
    });
   
});
