;

// Utility for older browsers, see 30 days to jQuery 15:02 into plugin lesson

if (typeof Object.create !== 'function') {
	Object.create =  function(obj) {
		function F() {};
		F.prototype = obj;
		return new F();
	};
}

// plugin loader

(function($, window, document, undefined){ // todo - do I need window, document?
	var loader,
		packManifest;

	$.fn.loader = function(options) {
        return this.each(function() {
            new Loader(this, options);
        })
    }; // end $.fn.loader

	$.fn.loader.options = {
	 	wrapEachWith: '<li></li>',
	 	template: "<ul data-dir='{{directory}}'>{{packName}}</ul>\n",
	 	transition: 'fadeToggle'
	 	// onComplete: null, // function to log?
	 }; // end $.fn.loader.options

	function Loader(element, options) {
		loader = this;
		loader.init(element, options);
	} // end of constructor

  	Loader.prototype.init = function(element, options) {
		loader.element = element;
		loader.$element = $(element);

		loader.processOptions(options);		// any user defind options
		loader.processManifest ();			// parse and store the manifest of packs
		loader.createPackList();			// create complete pack list
		loader.loadCurrentPack();

		loader.attachHandlers();			// attach all the handlers in one place

		// $("body").trigger("loadSlides", currentPack);
	};

	// process any user configurable options, if any passed in by user

	Loader.prototype.processOptions = function(options) {
		(typeof options === "string") 
			? console.log ("options should be an object, not a string")
			: loader.options = $.extend({}, $.fn.loader.options, options);
	}; // end processOptions


	// Pack Manifest section - holds meta data of all packs installed/purchased

	// processManifest
	// get the manifest (json) and parse it before storing it away into options.PackManifest

	Loader.prototype.processManifest = function () {
  		var packManifestJSON = this.getURLSynchronously (); 
        loader.options.PackManifest = $.parseJSON (packManifestJSON).manifest;
  	};

  	Loader.prototype.loadCurrentPack = function() {
  		var currentPack, slideList;
  		currentPack = loader.getCurrentPack();
  		slideList = loader.generateSlideList(currentPack);
  		loader.writeSlideList(slideList);
  		loader.loadSlideContent(currentPack);
  	};

  	// createPackList
  	// create a list of all packs 

	Loader.prototype.createPackList = function() {
		// console.log ("entered createPackList");
		var i, packList = [];

		for (i=0; i<loader.options.PackManifest.length; i++) {
			packList[i] = {
				packName: (loader.options.PackManifest[i].packName),
				directoryName: (loader.options.PackManifest[i].directoryName)
			};
			// console.log("Processed createPacklist " + packList[i].packName);
		};

		Master.packList = packList;
		return packList;
	};

	// getCurrentPack
	// Dummy function, this will get the last pack used and which slide last displayed

	Loader.prototype.getCurrentPack = function() {
		// var pack;

		if (Master.directory) {
			return {
				numberOfSlides: 3,
	        	directory: Master.directory,
				pack: "sublime getCurrentPack",
				currentSheet: 2
			};
		} else {
			return {
				numberOfSlides: 3,
	        	directory: "evernote",
				pack: "sublime getCurrentPack",
				currentSheet: 2
			};
		};
	}

	// generateSlideList
	// Generate a <ul> of slides in the current pack to be loaded by the slider

	Loader.prototype.generateSlideList = function (pack) {
        var prefix = "<ul>\n", // todo - check use of jQuery wrap elelemnt??? jQuery Tuts+ course
            postfix = "</ul>\n",
            slideSet,
            i,
            template = '<li><div class="sheet sheet{{i}}"' +
            	'data-file="slides/{{directory}}/index-0{{i}}.html"></div></li>\n';

        slideSet= prefix;
        for (i = 0; i < pack.numberOfSlides; i++) {

        	slideSet+= template.
        		replace( /{{directory}}/ig, pack.directory).
        		replace( /{{i}}/ig, i);
        }
        slideSet+= postfix;

        console.log("slideSet is " + slideSet);

        return slideSet;
    }

    // writeSlideList
    // write html <ul> of slides in current Pack to index.html element

    Loader.prototype.writeSlideList = function(slideSet) {
    	$("div#js-tokslider").html(slideSet);	// todo make element a passed in option
    }

    // loads the slide content from HTML list

    Loader.prototype.loadSlideContent = function (slideSet) {
        var i;
        console.log("entered loadSlideContent");
        console.log("number of slides = " + slideSet.numberOfSlides);

        for (i = 0; i < slideSet.numberOfSlides; i++) {
        	console.log("Loading Ajax content for slide " + i);
            sheet = ".sheet" + i;
            file = "slides/" + slideSet.directory + "/index-0" + i + ".html";
            $(sheet).load(file);
        }
    };

	// console.log ("Current pack is " + loader.getCurrentPack());

	Loader.prototype.attachHandlers = function () {

		// load a new pack, 
		
		loader.$element.find("ul").on("click", function (event) {
			event.stopPropagation();
			console.log("clicked " + $(this).data('dir'));
		});

		$("body").on('reloadPack',function(event, data){ 
			if (Master.directory) {
				console.log("reloading from the " + Master.directory);

				// sliderInstance = null;

				loader.loadCurrentPack();
				// sliderElement = $("div#js-tokslider").css('overflow', 'hidden').children('ul');

    // 			sliderInstance = sliderElement.tokslider(sliderElement, $("#js-tokslider-nav"));

				$("body").trigger("resize"); // move this to loadCurrentPack?
			}
			console.log("reload event occurred");
		});


		//keep the code below for refactoring and doing away with global
		// master object

		// $("body").on('reloadPack',function(event, data){ 
		// 	if (data.directory) {
		// 		console.log("reloading from the " + data.directory);
		// 	}
		// 	console.log("reload event occurred");
		// });
	}

  	Loader.prototype.getURLSynchronously = function() { // todo move to tok.utility plugin?
        return $.ajax({
            type: "GET",
            url: "manifest.json", //todo - make config var
	        cache: false,   // todo do I need to cache?, probbaly not for pack manifests
            async: false,
            // dataType: // todo don't seem to need this, investigate
        }).responseText;
    };

  	

})(jQuery, window, document);

// processManifest: open the local json manifest, parse, format

	// Loader.prototype.processManifest = function () {
	// 	loader.getManifest();
	// 	// loader.outputManifest();
	// 	// loader.formatManifest();
	// 	// loader.drawPackMenu();
	// }; // end processManifest


// To implement and scratch space

// NO Executable code beyond this point

// Loader.prototype.outputManifest = function () {
// 		var i;

// 		console.log("Number of packs = " + loader.options.PackManifest.length);
// 		for (i=0;i<loader.options.PackManifest.length;i++) {
// 			// console.log("Manifest is " + loader.options.PackManifest[i]);
// 			console.log("Pack Name " + loader.options.PackManifest[i].packName);
// 	    	console.log("Containing this many sheets: " + loader.options.PackManifest[i].numberOfSheets);
// 		}
// 	};

// Loader.prototype.regenerateManifest = function () {
// 		// todo P2, iterate throught the packs and rebuild
// 		// do I really need this - probably not?
// 	}; // end regenerateManifest


	// formatManifest() create html output

	// Loader.prototype.formatManifest = function () {
	// 	var i, packList = "";

	// 	for (i=0;i<loader.options.PackManifest.length;i++) {
	// 		packList += loader.options.template.
	// 			replace( /{{directory}}/ig, loader.options.PackManifest[i].directoryName).
	// 			replace( /{{packName}}/ig, loader.options.PackManifest[i].packName);
	// 	}

	// 	console.log(packList);
	// 	loader.$element
	// };

	// Loader.prototype.drawPackMenu = function() {
	// 	console.log ("entered drawpackMenu");
	// 	var i, packList = "", template = 
	// 		'<div class="stack" data-dir="{{directory}}">' +
	// 			'<img src="images/image4.jpg"></img>' +
	// 		'</div>\n';

	// 	for (i=0; i<loader.options.PackManifest.length; i++) {
	// 		packList += template.
	// 			replace( /{{directory}}/ig, loader.options.PackManifest[i].directoryName);
	// 	};
	// 	console.log("template is " + template);
	// 	console.log(packList);
	// 	$("#stacksContainer").append(packList);
	// };



	// $.fn.reloadPack = function () {
	// 	console.log ("hello in loader called");
	// 	return this;
	// };




