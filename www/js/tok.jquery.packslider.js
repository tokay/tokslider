; (function ($) {

    // Set any default settings

    var slider,     // convenience for this object
        viewport = {},
        config, 
        defaults = {
            fadeOutTimer: 300,
            fadeInTimer: 300
        };

    $.fn.packSlider = function(container, nav, options) {
        return this.each(function() {
            new PackSlider(container, nav, options);
        });
    }; // end $.fn.tokslider

    // Constructor

    function PackSlider(container, nav, options) {
        slider = this;
        config = $.extend({}, defaults, options);

        // setup configuration parameters
        config.container      = container;
        config.sheetList      = config.container.find('li ');
        config.numberOfSheets = config.sheetList.length;
        config.currentSheet   = 0;

        config.nav            = nav;
        
        slider.init();
    } // end of constructor

    // Init

    PackSlider.prototype.init = function() {
        slider.doSliderResize();
        slider.setHandlers();

        tokDebugMsg(3,"current sheet: " + config.currentSheet);
        tokDebugMsg(3,"margin-left:   " + -(config.currentSheet * config.sheetWidth));
        tokDebugMsg(3,"current width: " + config.sheetWidth);
    } // end of init


    PackSlider.prototype.doSliderReconfiguration = function() {
        config.container      = $("div#js-tokslider").css('overflow', 'hidden').children('ul');
        config.sheetList    = config.container.find('li ');
        config.numberOfSheets = config.sheetList.length;
        config.currentSheet   = 0;

        // slider.doSliderResize();
    };

    // Event Handlers - resize, orientation, swipe, tap, keyboard

    PackSlider.prototype.setHandlers = function() {
        var element = $("#js-tokslider");

        $(window).on('orientationchange', slider.doSliderResize());
        $(window).resize(function() { slider.doSliderResize(); });

        // slider.setResizeHandlers();
        slider.setSwipeHandlers(element);
        slider.setClickHandlers(element);
        slider.setTapHandlers(element);
        slider.setKeyboardHandlers();

        // forces 
        
        $("body").on("reloadPack", function () {
            console.log("reload called in slider");
            slider.doSliderReconfiguration();

        });
    };

    // Resize handlers fired by any event changing screen oreintation or 
    // windows size (eg browser resize on a desktop if permitted)

    // PackSlider.prototype.setResizeHandlers = function() {
    //     $(window).on('orientationchange', slider.doSliderResize());
    //     $(window).resize(function() { slider.doSliderResize(); });
    // };

    // Attach the keyboard event handlers:
    //      left and right arrow keys to move backwards and forwards

    PackSlider.prototype.setKeyboardHandlers = function() {
        $(document).keydown(function (e) {
            switch (e.keyCode) {
                case 37:            // left arrow (prev)
                    previousSlide();
                    tokDebugMsg(3, "left arrow key pressed");
                break;
                case 39:            // right arrow key (next)
                    nextSlide();
                    tokDebugMsg(3, "right arrow key pressed");
                break;
                default:
                    return false;
            }
            return false;
        });
    };

    // add convience functions for handlers eg
    // nextSlide, previousSlide

    var nextSlide = function() {
        slider.moveSheet("next");
    };

    var previousSlide = function() {
        slider.moveSheet("prev");
    };

    // Attach the swipe event handlers:
    //      swipes to move backwards and forwards

    PackSlider.prototype.setSwipeHandlers = function(element) {
        element.addSwipeEvents().on('swipeleft', nextSlide);
        element.addSwipeEvents().on('swiperight', previousSlide);
        // });
    }

    // Attach the click event handlers:
    //      swipes to move backwards and forwards
    //      any slide carousel related events

    PackSlider.prototype.setTapHandlers = function(element) {
        element.addSwipeEvents().on('tap', function (evt, touch) {
            tokDebugMsg(3, "tap");
        });
        element.addSwipeEvents().on('doubletap', function (evt, touch) {
            tokDebugMsg(3, "doubletap");
        });
    }

    // Attach the click event handlers:
    //      swipes to move backwards and forwards
    //      any slide carousel related events

    PackSlider.prototype.setClickHandlers = function(element) {

        // single click - next slide (optional, could repurpose this event for something else)

        element.addSwipeEvents().on('click', function (evt, touch) {

            slider.moveSheet("next");
        });

        // element.addSwipeEvents().on('doubleclick', function (evt, touch) {
        //     $('#debugPanel').toggle();
        //     // slider.orientationHandler();
        // });

        // $(".menubutton-left").on('click', function() {
        //     console.log("left menu button clicked");
        // });

        // $(".menubutton-right").on('click', function() {
        //     console.log("right menu button clicked");
        // });

        $(".debugger").on('click', function() {
            $('#debugPanel').toggle();
            console.log("debugger activated");
        });
    }
    

    // End of Event Handlers - resize, orientation, swipe, tap, keyboard

    // Elelemnt and component resizing 

    PackSlider.prototype.doSliderResize = function() {
        // slider.hideSlide();
        slider.getScreenDimensions();
        slider.setSliderDimensions();
        // slider.showSlide();

        tokDebugMsg(3, "current sheet: " + config.currentSheet);
        tokDebugMsg(3, "margin-left:   " + -(config.currentSheet * config.sheetWidth));
        tokDebugMsg(3, "current width: " + config.sheetWidth);
        tokDebugMsg(3, "doSliderResize event");
    }

    PackSlider.prototype.getScreenDimensions = function() {
        config.deviceHeight = window.innerHeight;
        config.deviceWidth = window.innerWidth;
        tokDebugMsg(3, "window inner height = " + config.deviceHeight);
        tokDebugMsg(3, "window inner width =  " + config.deviceWidth);
    }

    PackSlider.prototype.setSliderDimensions = function() {
        var height = config.deviceHeight, // - 20,
            width = config.deviceWidth; // - 20;

        config.sheetWidth = width;

        // todo tidy this up and get the explcit CSS out of here if possible

        $("#js-tokslider").css({
            "height": height,
            "width": width,
        });
        
        $(".sheet").css({
            "height": height, 
            "width": width
        });
        
        config.container.css({
            'margin-left': - (config.currentSheet * config.sheetWidth)
        });

        slider.refreshCurrentSheet();

        tokDebugMsg(3, "setSliderDimensions() new height = " + height);
        tokDebugMsg(3, "setSliderDimensions() new width = " + width);
    }
    
    PackSlider.prototype.moveSheet = function(direction) {
        console.log("move sheet called");
        $(".currentSlide").text(config.currentSheet);
        this.setCurrentSheet(direction);
        this.transition();
    }
   
    PackSlider.prototype.transition = function( coords ) {
        config.container.animate({
            // 'margin-left': coords || - (config.currentSheet * config.sheetWidth)
            'margin-left':  - (config.currentSheet * config.sheetWidth)

        });
        tokDebugMsg(3,"current sheet: " + config.currentSheet);
        tokDebugMsg(3,"margin-left:   " + -(config.currentSheet * config.sheetWidth));
        tokDebugMsg(3,"current width: " + config.sheetWidth);
    }

    PackSlider.prototype.setCurrentSheet = function( dir ) {
        var pos = config.currentSheet;
        console.log("pos = " + pos);
        
        pos += ( ~~( dir === 'next' ) || -1 );
        config.currentSheet = ( pos < 0 ) ? config.numberOfSheets - 1 : pos % config.numberOfSheets;
        return pos;
    }

    PackSlider.prototype.refreshCurrentSheet = function() {
        var pos = config.currentSheet;
        config.currentSheet = ( pos < 0 ) ? config.numberOfSheets - 1 : pos % config.numberOfSheets;
        return pos;
    }

}(jQuery));

// Hold stuff,  

// Tokslider.prototype.orientationHandler = function() {
    //     tokDebugMsg(3, "orientationHandler called");
    //     slider.doSliderResize();
    // }


 // Tokslider.prototype.redrawSheet = function( coords ) {
    //     config.container.fadeToggle();
    //     config.container.fadeToggle();
    // }



// Tokslider.prototype.getOrientation = function() {
    //     config.orientation = (window.innerHeight > window.innerWidth) ? "portait" : "landscape";
    // } 


// Tokslider.prototype.getOrientation = function () {
    //     config.oreintation = (config.deviceHeight > config.deviceWidth) ? "portait" : "landscape";
    //     tokDebugMsg(3, "Orientation is " + config.oreintation);
    // }

    // moveSheet - moves sheet in direction specfied
    // called by handlers for key, swipe, and other events

    // do I need this?

    // Tokslider.prototype.updateSliderDimensions = function() {
    //     slider.setSliderDimensions();
    // }

    // Tokslider.prototype.hideSlide = function() {
    //     config.container.hide();
    //     // config.container.fadeOut(config.fadeOutTimer);
    // }

    // Tokslider.prototype.showSlide = function() {
    //     config.container.show();
    //     // config.container.fadeIn(config.fadeInTimer);
    // }

    // Tokslider.prototype.getViewportSpecifications = function() {
    //     // config.deviceHeight = window.innerHeight;
    //     // config.deviceWidth = window.innerWidth;


    //     if (window.innerHeight > window.innerWidth) {
    //         viewport.Height = window.innerHeight;
    //         viewport.Width = window.innerWidth;
    //     } else {
    //         viewport.Height = window.innerHeight;
    //         viewport.Width = window.innerWidth;
    //     }
    //     tokDebugMsg(3, "viewport height = " + viewport.height);
    //     tokDebugMsg(3, "viewport width = " + viewport.width)

    // }   

    // Tokslider.prototype.setScreenDimensions = function() {
    //     var height = config.deviceHeight - 20,
    //         width = config.deviceWidth - 20;

    //     config.sheetWidth = width;

    //     $("#js-tokslider").css({ "height": height, "width": width});
    //     $(".sheet").css({ "height": height, "width": width});
       
    //     // slider.refreshCurrentSheet();

    //     tokDebugMsg(3, "setScreenDimensions() new height = " + height);
    //     tokDebugMsg(3, "setScreenDimensions() new width = " + width);
    // }

