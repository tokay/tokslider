;

// plugin logger

(function($, window, document, undefined){ // todo - do I need window, document?
	var logger;

	 $.fn.logger = function(options) {
        return this.each(function() {
            new Logger(this, options);
        })
    }; 

	$.fn.logger.options = {
		logDevice = html,	// console, alert, html
		logLevel = 3,

		tokDEBUG_INFORMATION = 3,
tokDEBUG_ERROR = 2,
tokDEBUG_WARNING = 1,
tokDEBUG_OFF = 0,
tokDEBUG_LEVEL = tokDEBUG_OFF;



	 	transition: 		'fadeToggle',
	 	transitionTimer: 	1500,
	 }; // end $.fn.logger.options
	 
	function Logger(element, options) {
		logger = this;
		logger.init(element, options);
	} // end of constructor

	// init:
	//		init variables
	//		process user options and defaults
	//		draw (hidden) pack menu
	//		attach event handlers

  	Logger.prototype.init = function(element, options) {
		logger.element = element;
		logger.$element = $(element);

		logger.processOptions(options);
		logger.drawLogger();
		logger.attachHandlers();
	};

	// process configurable options, if any passed in by user

	Logger.prototype.processOptions = function(options) {
		(typeof options === "string") 
			? console.log ("options should be an object, not a string")
			: logger.options = $.extend({}, $.fn.logger.options, options);
	}; // end processOptions
	
	// generate the <div> list of packs and their icons for rendering in packMenu
	// todo - this should be generate not draw - sepereate these 2 functions

	Logger.prototype.drawPackMenu = function() {
		var i, packList = "", 
			template = 
				'<div class="stack" data-dir="{{directory}}">' +
					'<img class="packIcon" src="slides/{{directory}}/menuicon.jpg"></img>' +
				'</div>\n';

		for (i = 0; i < Master.packList.length; i++) {
			packList += template.
				replace( /{{directory}}/ig, Master.packList[i].directoryName);
		};

		$("#stacksContainer").append(packList);
	};



})(jQuery, window, document);