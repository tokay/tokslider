var tokDEBUG_DEVICE= "html", // console, alert, html
tokDEBUG_INFORMATION = 3,
tokDEBUG_ERROR = 2,
tokDEBUG_WARNING = 1,
tokDEBUG_OFF = 0,
tokDEBUG_LEVEL = tokDEBUG_OFF;

// tokDEBUG_DEVICE="console";

// Debug function
//
// Devices console, alert, could add logfile
// Severity:
// 1: Severe 
// 2: Warning
// 3: Information
// 
// todo: add date/time stamp

function tokDebugMsg (level, msg) {
	var debugMsg, debugTemplate = "DEBUG, {{level}} {{msg}}";

	debugMsg = debugTemplate.replace( /{{level}}/ig, level).replace(/{{msg}}/ig, msg);

    // debugMsg = level + debugPrefix + msg;

	if (level === tokDEBUG_OFF  || level < tokDEBUG_LEVEL) { // no debug
	    return;
	}

	if (tokDEBUG_DEVICE === "console") {
	    console.log (debugMsg);
	} else if (tokDEBUG_DEVICE === "alert") {
	    alert (debugMsg);
	} else if (tokDEBUG_DEVICE === "html") {
	    $(".tokDEBUGLOG").prepend("<li>" + debugMsg + "</li>");
	}
};

// tokSetDebugLevel
// 
// set level of debugging

function tokSetDebugLevel (level) {
if (level) {
    tokDEBUG_LEVEL = level;
}
return tokDEBUG_LEVEL;  
}