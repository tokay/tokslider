;

// plugin packMenu

(function($, window, document, undefined){ // todo - do I need window, document?
	var packMenu;

	// $.fn.packMenu = function(element, options) {
 //        return this.each(function() {
 //            new PackMenu(element, options);
 //        })
 //    }; // end $.fn.packMenu

 $.fn.packMenu = function(options) {
        return this.each(function() {
            new PackMenu(this, options);
        })
    }; 

	$.fn.packMenu.options = {
	 	transition: 		'fadeToggle',
	 	transitionTimer: 	5000,
	 }; // end $.fn.packMenu.options

	function PackMenu(element, options) {
		packMenu = this;
		packMenu.init(element, options);
	} // end of constructor

	// init:
	//		init variables
	//		process user options and defaults
	//		draw (hidden) pack menu
	//		attach event handlers

  	PackMenu.prototype.init = function(element, options) {
		packMenu.element = element;
		packMenu.$element = $(element);

		packMenu.processOptions(options);
		packMenu.drawPackMenu();
		packMenu.attachHandlers();
	};

	// process configurable options, if any passed in by user

	PackMenu.prototype.processOptions = function(options) {
		(typeof options === "string") 
			? console.log ("options should be an object, not a string")
			: packMenu.options = $.extend({}, $.fn.packMenu.options, options);
	}; // end processOptions
	
	// generate the <div> list of packs and their icons for rendering in packMenu
	// todo - this should be generate not draw - sepereate these 2 functions

	PackMenu.prototype.drawPackMenu = function() {
		var i, packList = "", 
			template = 
				'<div class="stack" data-dir="{{directory}}">' +
					'<img class="packIcon" src="slides/{{directory}}/menuicon.jpg"></img>' +
				'</div>\n';

		for (i = 0; i < Master.packList.length; i++) {
			packList += template.
				replace( /{{directory}}/ig, Master.packList[i].directoryName);
		};

		$("#stacksContainer").append(packList);
	};

	// Attach packMenu related handlers:
	// 		Get pack when menu item (div, img etc) clicked
	//		Activate/Dismiss packMenu when menu selector clicked

	PackMenu.prototype.attachHandlers = function () {

		// todo clean up this global object and set up message/object pass

		// listen for pack selection from the menu of packs

		$(".stack").on("click", function (event){
			event.stopPropagation();
			Master.directory = $(this).data('dir'); // todo message pass?
			console.log("PackMenu attachHandlers sends reloadPack " + Master.currentPack);

			//todo pass a pack object, stop using global var

			$("body").trigger("reloadPack", { directory: Master.currentPack});
			(packMenu.$element)[ packMenu.options.transition ]( packMenu.options.transitionTimer );
		});

		// listen for the menu button to be clicked

		$("img#menubutton-left").on("click", function (event) {
			// console.log("menu button clicked");
			(packMenu.$element)[ packMenu.options.transition ]( packMenu.options.transitionTimer );
        	event.stopPropagation();
    	});

    	$("img#menubutton-right").on("click", function (event) {
			// console.log("debug button clicked")
			$("#debugPanel")[ packMenu.options.transition ]( packMenu.options.transitionTimer );
        	event.stopPropagation();
    	});

		$("#menuButton").on("click", function (event) {
			(packMenu.$element)[ packMenu.options.transition ]( packMenu.options.transitionTimer );
        	event.stopPropagation();
    	});

    	$("#stacksContainer").on("click", function (event) {
			(packMenu.$element)[ packMenu.options.transition ]( packMenu.options.transitionTimer );
        	event.stopPropagation();
    	});
	}

})(jQuery, window, document);