$(function() {

    // gerate HTML for slide list

    function generateSlideList (numberOfSlides) {
        var prefix = "<ul>\n",
            postfix = "</ul>\n",
            payload,
            i;

        payload = prefix;
        for (i = 0; i < numberOfSlides; i++) {
            payload += '<li><div class="sheet sheet' 
            payload += i;
            payload += '" data-file="slides\/index-0';
            payload += i;
            payload += '.html">';
            payload += '</div></li>\n';
        }
        payload += postfix;

        return payload;
    }

    // dummy pack

    var pack = {
        numberOfSlides: 9,
        directory: "sublime"
    };

    // enet handler function for loading a pack

    var theLoader = function (event, pack) {
        if (pack.directory && pack.numberOfSlides) {
            console.log("reload " + pack.numberOfSlides + " from the " + pack.directory);
        }
        console.log("I am the loader");
    };

    // $("body").bind('loadSlides', theLoader);

    $("body").bind("loadSlides", function(event, pack) {
        console.log("new loader loaded");
    })
    $("body").trigger("loadSlides", pack);

    // load slide list

    function loadSlider (numberOfSlides) {
        var file, sheet, i;

        for (i = 0; i < numberOfSlides; i++) {
            sheet = ".sheet" + i;
            file = "slides/index-0" + i + ".html";
            $(sheet).load(file);
        }
    }

    // generate a list (li) of slides

    $("#js-tokslider").html(generateSlideList(2));

    // init the slider, hiding the list
    var tSlider = $("div#js-tokslider").css('overflow', 'hidden').children('ul');
    // init the nav

    tSlider.tokslider(tSlider, $("#js-tokslider-nav"));

    // load the slides

    $("#js-tokslider").html(loadSlider(2));  //todo make this depend on generateSlideList

    var Master = {              // todo is there a better way than using this gloabl object?
        packList:  null,
        // currentPack: null
    };


    // load the manifest

    $("ul.loader").loader($("ul.loader"));
    
    // init the main PackMenu

    $(".packMenu").packMenu($(".packMenu"));
});

