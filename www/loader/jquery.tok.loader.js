;

// Utility for older browsers, see 30 days to jQuery 15:02 into plugin lesson

if (typeof Object.create !== 'function') {
	Object.create =  function(obj) {
		function F() {};
		F.prototype = obj;
		return new F();
	};
}

// plugin loader
//

(function($, window, document, undefined){ // todo - do I need window, document?
	var loader,
		packManifest;

	$.fn.loader = function(element, options) {
        return this.each(function() {
            new Loader(element, options);
        })
    }; // end $.fn.loader

	$.fn.loader.options = {
	 	wrapEachWith: '<li></li>',
	 	template: "<ul data-dir='{{directory}}'>{{packName}}</ul>\n",
	 	transition: 'fadeToggle'
	 	// onComplete: null, // function to log?
	 }; // end $.fn.loader.options

	function Loader(element, options) {
		loader = this;
		loader.init(element, options);
	} // end of constructor

  	Loader.prototype.init = function(element, options) {
		loader.element = element;
		loader.$element = $(element);

		loader.processOptions(options);
		loader.processManifest ();
		loader.attachHandlers();
		loader.createPackList();

	};

	Loader.prototype.hello = function () {
		console.log ("hello");
		return 8;
	}

	// process configurable options, if any passed in by user

	Loader.prototype.processOptions = function(options) {
		(typeof options === "string") 
			? console.log ("options should be an object, not a string")
			: loader.options = $.extend({}, $.fn.loader.options, options);
	}; // end processOptions

	// Manifest section 
	// Manifiest file (json) holds a list of all packs, number of sheets, and
	// where to find them

	Loader.prototype.getManifest = function(file) {
  		var packManifestJSON = this.getURLSynchronously (); 
        loader.options.PackManifest = $.parseJSON (packManifestJSON).manifest;
        // console.log(loader.options.PackManifest[0].packName);
  	};

	// processManifest: open the local json manifest, parse, format

	Loader.prototype.processManifest = function () {
		loader.getManifest();
		loader.outputManifest();
		// loader.formatManifest();
		// loader.drawPackMenu();
	}; // end processManifest

	Loader.prototype.createPackList = function() {
		console.log ("entered createPackList");
		var i, packList = [];

		for (i=0; i<loader.options.PackManifest.length; i++) {
			packList[i] = {
				packName: (loader.options.PackManifest[i].packName),
				directoryName: (loader.options.PackManifest[i].directoryName)
			};
			console.log("Processed createPacklist " + packList[i].packName);
		};

		Master.packList = packList;
		return packList;
	};

	// formatManifest() create html output

	Loader.prototype.formatManifest = function () {
		var i, packList = "";

		for (i=0;i<loader.options.PackManifest.length;i++) {
			packList += loader.options.template.
				replace( /{{directory}}/ig, loader.options.PackManifest[i].directoryName).
				replace( /{{packName}}/ig, loader.options.PackManifest[i].packName);
		}

		console.log(packList);
		loader.$element
	};

	Loader.prototype.drawPackMenu = function() {
		console.log ("entered drawpackMenu");
		var i, packList = "", template = 
			'<div class="stack" data-dir="{{directory}}">' +
				'<img src="images/image4.jpg"></img>' +
			'</div>\n';

		for (i=0; i<loader.options.PackManifest.length; i++) {
			packList += template.
				replace( /{{directory}}/ig, loader.options.PackManifest[i].directoryName);
		};
		console.log("template is " + template);
		console.log(packList);
		$("#stacksContainer").append(packList);
	};

	Loader.prototype.attachHandlers = function () {
		
		loader.$element.find("ul").on("click", function (event) {
			event.stopPropagation();
			console.log("clicked " + $(this).data('dir'));
		});

		// $("body").on("click", function(event) {
		// 	console.log("body click");
		// })
	}

	Loader.prototype.outputManifest = function () {
		var i;

		console.log("Number of packs = " + loader.options.PackManifest.length);
		for (i=0;i<loader.options.PackManifest.length;i++) {
			// console.log("Manifest is " + loader.options.PackManifest[i]);
			console.log("Pack Name " + loader.options.PackManifest[i].packName);
	    	console.log("Containing this many sheets: " + loader.options.PackManifest[i].numberOfSheets);
		}
	};

  	Loader.prototype.getURLSynchronously = function() { // todo move to tok.utility plugin?
        return $.ajax({
            type: "GET",
            url: "manifest.json", //todo - make config var
	        cache: false,   // todo do I need to cache?, probbaly not for pack manifests
            async: false,
            // dataType: // todo don't seem to need this, investigate
        }).responseText;
    };

  	

})(jQuery, window, document);


// To implement and scratch space

// NO Executable code beyond this point

// Loader.prototype.regenerateManifest = function () {
// 		// todo P2, iterate throught the packs and rebuild
// 		// do I really need this - probably not?
// 	}; // end regenerateManifest



