// Utility for older browsers, see 30 days to jQuery 15:02 into plugin lesson

if (typeof Object.create !== 'function') {
	Object.create =  function(obj) {
		function F() {};
		F.prototype = obj;
		return new F();
	};
}

(function($, window, document, undefined){
	var Loader = {
		init: function(options, elem) {
			var i, self = this;
			self.elem = elem;
			self.$elem = $(elem);

			self.var = "tmp example";
			greeting = "I was set in loader";

			if (typeof options === "string") {
				// placeholder
				console.log ("options should be an object, not a string");
			} else {
				self.options = $.extend({}, $.fn.loader.options, options)
			}
			console.log("loader init called");
			var packManifest = self.getManifest ();

			for (i=0;i<packManifest.length;i++) {
				console.log("Manifest is " + packManifest[i]);
				console.log("Do we have manifest? " + packManifest[i].packName);
		    	console.log("Do we have manifest? " + packManifest[i].numberOfSheets);
			}
  
		},

		getURLSynchronously: function () {
	        return $.ajax({
	            type: "GET",
	            url: "manifest.json",
		        cache: false,   // do I need to cache?, probbaly not for pack manifests
	            async: false
	        }).responseText;
	    },

	     getManifest: function (file) {
	        var packManifestJSON = this.getURLSynchronously (); 
	        var parsedPackManifest = $.parseJSON (packManifestJSON);
	        return parsedPackManifest.manifest;
	    },

  	};

	$.fn.loader = function (options) {
		return this.each(function() {

			var loader = Object.create(Loader);
			loader.init(options, this);

			console.log("loader called");
		});
	};

	// $.fn.loader.options = {
	// 	dummy: "dummy option"

	// }


})(jQuery, window, document);



