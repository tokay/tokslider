; (function ($) {
    "use strict";

    // Set any default settings

    var slider,     // convenience for this object
        viewport = {},
        config, 
        defaults = {
            fadeOutTimer: 300,
            fadeInTimer: 300
        };

    // Constructor

    function Tokslider(container, nav, options) {
        slider = this;
        config = $.extend({}, defaults, options);

        // setup configuration parameters
        config.container      = container;
        config.sheetList      = config.container.find('li ');
        config.numberOfSheets = config.sheetList.length;
        config.currentSheet   = 0;

        config.nav            = nav;

        // config.currentSlide   = 0;
        // config.sheetWidth     = 1004;
        
        slider.init();
    } // end of constructor

    // Init

    Tokslider.prototype.init = function() {
        slider.doSliderResize();
        slider.setHandlers();

        tokDebugMsg(3,"current sheet: " + config.currentSheet);
        tokDebugMsg(3,"margin-left:   " + -(config.currentSheet * config.sheetWidth));
        tokDebugMsg(3,"current width: " + config.sheetWidth);
    } // end of init

    // Handlers
    //
    // resize, orientation, swipe, tap, keyboard

    Tokslider.prototype.setHandlers = function() {
        var element = $("#js-tokslider");
        
        console.log("this is: " + this);
        console.log("element: " + element);
        console.log("element: " + element);

        $(window).on('orientationchange', slider.doSliderResize());
        $(window).resize(function() {slider.doSliderResize(); });
    
        slider.setSwipeHandlers(element);
        slider.setClickHandlers(element);
        slider.setTapHandlers(element);
        slider.setKeyboardHandlers();
    }

    Tokslider.prototype.setKeyboardHandlers = function() {
        $(document).keydown(function (e) {
            switch (e.keyCode) {
                case 37:            // left arrow (prev)
                    slider.moveSheet("prev");
                    // console.log("key handler - prev");
                break;
                case 39:            // right arrow key (next)
                    slider.moveSheet("next");
                    tokDebugMsg(3, "right arrow key pressed");
                break;
                default:
                    return false;
            }
            return false;
        });
    }

    Tokslider.prototype.setSwipeHandlers = function(element) {
        element.addSwipeEvents().on('swipeleft', function (evt, touch) {
            slider.moveSheet("next");
        });

        element.addSwipeEvents().on('swiperight', function (evt, touch) {
            slider.moveSheet("prev");
        });
    }

    Tokslider.prototype.setTapHandlers = function(element) {
        element.addSwipeEvents().on('tap', function (evt, touch) {
            tokDebugMsg(3, "tap");
        });
        element.addSwipeEvents().on('doubletap', function (evt, touch) {
            tokDebugMsg(3, "doubletap");
        });
    }

    Tokslider.prototype.setClickHandlers = function(element) {
        
        element.addSwipeEvents().on('click', function (evt, touch) {
            slider.moveSheet("next");
        });

        element.addSwipeEvents().on('doubleclick', function (evt, touch) {
            slider.orientationHandler();
        });
    }

    // End of handlers

    //  

    Tokslider.prototype.getOrientation = function() {
        config.orientation = (window.innerHeight > window.innerWidth) ? "portait" : "landscape";
    }

    Tokslider.prototype.doSliderResize = function() {
        slider.hideSlide();
        slider.getScreenDimensions();
        slider.setSliderDimensions();
        slider.showSlide();

        tokDebugMsg(3, "current sheet: " + config.currentSheet);
        tokDebugMsg(3, "margin-left:   " + -(config.currentSheet * config.sheetWidth));
        tokDebugMsg(3, "current width: " + config.sheetWidth);
        tokDebugMsg(3, "doSliderResize event");
    }

    Tokslider.prototype.getScreenDimensions = function() {
        config.deviceHeight = window.innerHeight;
        config.deviceWidth = window.innerWidth;

        tokDebugMsg(3, "window inner height = " + config.deviceHeight);
        tokDebugMsg(3, "window inner width =  " + config.deviceWidth);
    }

    Tokslider.prototype.setSliderDimensions = function() {
        var height = config.deviceHeight - 20,
            width = config.deviceWidth - 20;

        config.sheetWidth = width;

        $("#js-tokslider").css({            // resize slider container
            "height": height, 
            "width": width
        });
        $(".sheet").css({                   // resize sheet and align via margin-left
            "height": height, 
            "width": width,
            "margin-left": - (config.currentSheet * config.sheetWidth)
        });
        
        // config.container.css({
        // $(".sheet").css({
        //     'margin-left': - (config.currentSheet * config.sheetWidth)
        // });

        slider.refreshCurrentSheet();

        tokDebugMsg(3, "setSliderDimensions() new height = " + height);
        tokDebugMsg(3, "setSliderDimensions() new width = " + width);
    }

    Tokslider.prototype.orientationHandler = function() {
        tokDebugMsg(3, "orientationHandler called");
        slider.doSliderResize();
    }

    Tokslider.prototype.getOrientation = function () {
        config.oreintation = (config.deviceHeight > config.deviceWidth) ? "portait" : "landscape";
        tokDebugMsg(3, "Orientation is " + config.oreintation);
    }

    // moveSheet - moves sheet in direction specfied
    // called by handlers for key, swipe, and other events

    // do I need this?

    Tokslider.prototype.updateSliderDimensions = function() {
        slider.setSliderDimensions();
    }

    Tokslider.prototype.hideSlide = function() {
        config.container.hide();
        // config.container.fadeOut(config.fadeOutTimer);
    }

    Tokslider.prototype.showSlide = function() {
        config.container.show();
        // config.container.fadeIn(config.fadeInTimer);
    }

    Tokslider.prototype.moveSheet = function(direction) {
        $(".currentSlide").text(config.currentSheet);
        this.setCurrentSheet(direction);
        this.transition();

    }

    Tokslider.prototype.redrawSheet = function( coords ) {
        config.container.fadeToggle();
        config.container.fadeToggle();
    }

    Tokslider.prototype.transition = function( coords ) {
        config.container.animate({
            // 'margin-left': coords || - (config.currentSheet * config.sheetWidth)
            'margin-left':  - (config.currentSheet * config.sheetWidth)

        });
        tokDebugMsg(3,"current sheet: " + config.currentSheet);
        tokDebugMsg(3,"margin-left:   " + -(config.currentSheet * config.sheetWidth));
        tokDebugMsg(3,"current width: " + config.sheetWidth);
    }

    Tokslider.prototype.setCurrentSheet = function( dir ) {  

        var pos = config.currentSheet;

        pos += ( ~~( dir === 'next' ) || -1 );
        config.currentSheet = ( pos < 0 ) ? config.numberOfSheets - 1 : pos % config.numberOfSheets;
        return pos;
    }

    Tokslider.prototype.refreshCurrentSheet = function() {
        var pos = config.currentSheet;
        config.currentSheet = ( pos < 0 ) ? config.numberOfSheets - 1 : pos % config.numberOfSheets;
        return pos;
    }

    $.fn.tokslider = function(container, nav, options) {
        return this.each(function() {
            new Tokslider(container, nav, options);
        })
    }; // end $.fn.tokslider


    // Tokslider.prototype.getViewportSpecifications = function() {
    //     // config.deviceHeight = window.innerHeight;
    //     // config.deviceWidth = window.innerWidth;


    //     if (window.innerHeight > window.innerWidth) {
    //         viewport.Height = window.innerHeight;
    //         viewport.Width = window.innerWidth;
    //     } else {
    //         viewport.Height = window.innerHeight;
    //         viewport.Width = window.innerWidth;
    //     }
    //     tokDebugMsg(3, "viewport height = " + viewport.height);
    //     tokDebugMsg(3, "viewport width = " + viewport.width)

    // }   

    // Tokslider.prototype.setScreenDimensions = function() {
    //     var height = config.deviceHeight - 20,
    //         width = config.deviceWidth - 20;

    //     config.sheetWidth = width;

    //     $("#js-tokslider").css({ "height": height, "width": width});
    //     $(".sheet").css({ "height": height, "width": width});
       
    //     // slider.refreshCurrentSheet();

    //     tokDebugMsg(3, "setScreenDimensions() new height = " + height);
    //     tokDebugMsg(3, "setScreenDimensions() new width = " + width);
    // }

}(jQuery));