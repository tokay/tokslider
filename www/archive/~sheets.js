$(function() {

    function populateSheets (numberOfSlides) {
        var prefix = "<ul>\n",
            postfix = "</ul>\n",
            payload,
            i;

        payload = prefix;
        for (i = 0; i < numberOfSlides; i++) {
            payload += '<li><div class="sheet sheet' 
            payload += i;
            payload += '" data-file="slides\/index-0';
            payload += i;
            payload += '.html">';
            payload += '</div></li>\n';
        }
        payload += postfix;

        return payload;
    }

    function loadSheets (numberOfSlides) {
        var file, sheet, i;

        for (i = 0; i < numberOfSlides; i++) {
            sheet = ".sheet" + i;
            file = "slides/index-0" + i + ".html";
            $(sheet).load(file);
        }
    }
    
    $("#js-tokslider").html(populateSheets(2));

    var tSlider = $("div#js-tokslider").css('overflow', 'hidden').children('ul');
    tSlider.tokslider(tSlider, $("#js-tokslider-nav"));

    $("#js-tokslider").html(loadSheets(2));  //todo make this depend on populateSheets

    // $(".start").load("slides/load-0.html");
    // $(".one").load("slides/load-1.html");
    // $(".two").load("slides/load-2.html");
    // $(".three").load("slides/load-3.html");
    // $(".end").load("slides/load-end.html");
});